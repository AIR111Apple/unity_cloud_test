﻿using UnityEngine;
using System.Collections;

[RequireComponent  (typeof(CharacterController))]
public class Player : MonoBehaviour
{
	//Movement controls 
	public float movementSpeed = 25.0f;
	public float mouseSensitivity = 5.0f;
	public float upDownRange = 60.0f;
	//float verticalRotation = 0.0f;
	float verticalVelocity = 0.0f;
	public float jumpSpeed = 10.0f;
	public float moveSpeed = 6.0f;
	CharacterController characterController;
	
	
	
	
	
	void Start ()
	{
		characterControllerCheck ();
	}
	
	void Update ()
	{
		playerMove ();
		
		mouseSensitivityChange ();
		
		//death ();
		
		
		
	}
	
	
	
	
	void characterControllerCheck ()
	{
		characterController = GetComponent<CharacterController> ();
		
		if (characterController == null) {
			Debug.Log ("something missing");
		}
		
	}
	
	void playerMove ()
	{
		float rotLeftRight = Input.GetAxis ("Mouse X") * mouseSensitivity;
		
		transform.Rotate (0, rotLeftRight, 0);
		
		//verticalRotation -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		
		//verticalRotation = Mathf.Clamp (verticalRotation, -upDownRange, upDownRange);
		
		Camera.main.transform.localRotation = Quaternion.Euler (37, 0, 0);
		
		float forwardSpeed = Input.GetAxis ("Vertical") * movementSpeed;
		
		float sideSpeed = Input.GetAxis ("Horizontal") * movementSpeed;
		if (!characterController.isGrounded) {
			verticalVelocity += Physics.gravity.y * Time.deltaTime;
			
		} else {
			verticalVelocity = 0;
		}
		
		if (characterController.isGrounded && Input.GetButton ("Jump")) {
			verticalVelocity = jumpSpeed;
		}
		
		if (characterController.isGrounded && Input.GetKey ("left shift")) {
			movementSpeed = 30.0f;
		} else {
			movementSpeed = 25.0f;
		}
		
		
		
		Vector3 Speed = new Vector3 (sideSpeed, verticalVelocity, forwardSpeed);
		Speed = transform.rotation * Speed;
		
		characterController.Move (Speed * Time.deltaTime);
		
		
	}
	
	void mouseSensitivityChange ()
	{
		
		if (Input.GetKeyDown (KeyCode.LeftBracket)) {
			
			mouseSensitivity --;
			Debug.Log (mouseSensitivity);
			
		}
		if (Input.GetKeyDown (KeyCode.RightBracket)) {
			
			mouseSensitivity++;
			Debug.Log (mouseSensitivity);
		}
	}
	
	/*void death ()
	{
		if (transform.position.y <= -100) {
			Respawn ();
		}

	}

	private new Vector3 RespawnPos;

	void Respawn ()
	{
		this.transform.position = RespawnPos;

	}*/
	
}


